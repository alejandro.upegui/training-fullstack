import React from "react";
import ReactDOM from "react-dom";
import "./Styles/index.css";
import Form from "./components/Form/Form";

ReactDOM.render(
  <React.StrictMode>
    <Form />
  </React.StrictMode>,
  document.getElementById("root")
);
