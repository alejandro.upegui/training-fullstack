import React, { Component } from "react";
import Label from "../input/index";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Name: "",
      Description: "",
      BasePrice: "",
      TaxRate: "",
      ProductStatus: false,
      InventorQuantity: 0,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({ ...this.state, [event.target.name]: value });
  }
  render() {
    return [
      <div>
        <form action="" method="get" className="form">
          <Label type="text" field="Name">
            <input
              name="Name"
              type="text"
              className="form-control"
              placeholder="Name"
              onChange={this.handleChange}
              value={this.state.Name}
            />
          </Label>
          <Label type="text" field="Description">
            <input
              name="Description"
              type="text"
              placeholder="description"
              className="form-control"
              value={this.state.Description}
              onChange={this.handleChange}
            />
          </Label>
          <Label type="text" field="Base Price">
            <input
              name="BasePrice"
              type="text"
              placeholder="Base Price"
              className="form-control"
              // value={this.state.BasePrice}
              onChange={this.handleChange}
            />
          </Label>
          <Label type="text" field="Tax Rate">
            <input
              name="TaxRate"
              type="text"
              placeholder="Tax Rate"
              className="form-control"
              value={this.state.TaxRate}
              onChange={this.handleChange}
            />
          </Label>
          <Label type="text" field="Product status">
            <input
              name="ProductStatus"
              type="text"
              placeholder="Product status"
              className="form-control"
              value={this.state.ProductStatus}
              onChange={this.handleChange}
            />
          </Label>
          <Label type="text" field="Inventor quantity">
            <input
              name="InventorQuantity"
              type="text"
              placeholder="Inventor quantity"
              className="form-control"
              value={this.state.InventorQuantity}
              onChange={this.handleChange}
            />
          </Label>
          <button>submit</button>
        </form>
        ,
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th scope="col">Base Price</th>
              <th scope="col">Tax Rate</th>
              <th scope="col">Product status</th>
              <th scope="col">Inventor quantity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>{this.state.Name}</td>
              <td>{this.state.Description}</td>
              <td>{this.state.BasePrice}</td>
              <td>{this.state.TaxRate}</td>
              <td>{this.state.ProductStatus}</td>
              <td>{this.state.InventorQuantity}</td>
            </tr>
          </tbody>
        </table>
      </div>,
    ];
  }
}

export default Form;
