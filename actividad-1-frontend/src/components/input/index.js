import React from "react";
import "../Form/AppStyles.css";

function Label({ field, children, className }) {
  return (
    <div>
      <label htmlFor="" className={className} name={field}>
        {field}
        {children}
      </label>
    </div>
  );
}
export default Label;
