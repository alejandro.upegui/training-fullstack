package com.example.trainingfullstack.user.domain;

import com.example.trainingfullstack.common.Precondition;
import lombok.Value;

@Value(staticConstructor = "from")
public class UserAuthRequest {
    UserName username;
    String password;

    public UserAuthRequest(UserName username, String password) {
        Precondition.checkNotNull(username);
        Precondition.checkNotNull(password);
        this.username = username;
        this.password = password;
    }
}
