package com.example.trainingfullstack.user.domain;

import com.example.trainingfullstack.common.Precondition;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value(staticConstructor = "of")
public class UserName {

   String value;

   private UserName(String value){
       Precondition.checkNotNull(value);
       Precondition.checkArgument(StringUtils.isNoneBlank(value));
       Precondition.checkArgument(StringUtils.isNoneBlank(value));
       Precondition.checkArgument(value.length()>= 6);
       this.value=value;
   }
}
