package com.example.trainingfullstack.user.controller;

import com.example.trainingfullstack.user.domain.UserAuthRequest;
import com.example.trainingfullstack.user.domain.UserName;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    @GetMapping("/Hello")
    public String greeting(){
        return "Hello World";
    }

    @PostMapping("/authenticate")
    public UserAuthRequest authenticate(){
        UserName userName = UserName.of("test-username");
        String password = "password...";
        return UserAuthRequest.from(
            userName,
            password
        );

    }
}
