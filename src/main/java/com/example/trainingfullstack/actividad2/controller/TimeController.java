package com.example.trainingfullstack.actividad2.controller;

import com.example.trainingfullstack.actividad2.domain.HoraDelDia;
import com.example.trainingfullstack.actividad2.domain.Hour;
import com.example.trainingfullstack.actividad2.domain.Minute;
import com.example.trainingfullstack.actividad2.domain.Second;
import com.example.trainingfullstack.configuration.CredentialsUnitTime;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;

@RestController
@RequestMapping("/api/v1/fecha")
public class TimeController {
    CredentialsUnitTime credentialsUnitTime;

    public TimeController(CredentialsUnitTime credentialsUnitTime) {
        this.credentialsUnitTime = credentialsUnitTime;
    }

    @GetMapping("/tiempo")
    private HoraDelDia tiempo(){
        LocalTime time = LocalTime.now();
        Hour hour = Hour.of(time.getHour());
        Minute minute = Minute.of(time.getMinute());
        Second second = Second.of(time.getSecond());
        System.out.println(credentialsUnitTime.getUnitTime());
        return HoraDelDia.from(hour,minute,second);
    }
}
