package com.example.trainingfullstack.actividad2.serialization;

import com.example.trainingfullstack.actividad2.domain.Hour;
import com.example.trainingfullstack.user.domain.UserName;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class HourAdapter implements JsonSerializer<Hour> {

    @Override
    public JsonElement serialize(Hour hour, Type type, JsonSerializationContext jsonSerializationContext) {
        Integer value= hour.getValue();
        return new JsonPrimitive(value);
    }
}
