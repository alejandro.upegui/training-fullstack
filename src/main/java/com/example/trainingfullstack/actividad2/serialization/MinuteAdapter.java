package com.example.trainingfullstack.actividad2.serialization;

import com.example.trainingfullstack.actividad2.domain.Minute;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class MinuteAdapter implements JsonSerializer<Minute> {
    @Override
    public JsonElement serialize(Minute minute, Type type, JsonSerializationContext jsonSerializationContext) {
        Integer value = minute.getValue();
        return new JsonPrimitive(value);
    }
}
