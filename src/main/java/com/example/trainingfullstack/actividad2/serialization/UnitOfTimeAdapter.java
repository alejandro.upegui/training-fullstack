package com.example.trainingfullstack.actividad2.serialization;

import com.example.trainingfullstack.actividad2.domain.UnidadDeTiempo;
import com.example.trainingfullstack.actividad2.domain.UnidadDeTiempoEnum;
import com.google.gson.*;

import java.lang.reflect.Type;

public class UnitOfTimeAdapter implements JsonSerializer<UnidadDeTiempo>, JsonDeserializer<UnidadDeTiempoEnum> {
    @Override
    public JsonElement serialize(UnidadDeTiempo unidadDeTiempo, Type type, JsonSerializationContext jsonSerializationContext) {
        UnidadDeTiempoEnum value = unidadDeTiempo.getUnidadDeTiempoEnum();
        return new JsonPrimitive(String.valueOf(value));
    }

    @Override
    public UnidadDeTiempoEnum deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        String value = jsonElement.getAsString();
        return UnidadDeTiempoEnum.valueOf(value);
    }
}
