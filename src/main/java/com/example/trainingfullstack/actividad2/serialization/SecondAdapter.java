package com.example.trainingfullstack.actividad2.serialization;

import com.example.trainingfullstack.actividad2.domain.Second;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class SecondAdapter implements JsonSerializer<Second> {
    @Override
    public JsonElement serialize(Second second, Type type, JsonSerializationContext jsonSerializationContext) {
        Integer value = second.getValue();
        return new JsonPrimitive(value);
    }
}
