package com.example.trainingfullstack.actividad2.domain;

import com.example.trainingfullstack.common.Precondition;
import lombok.Value;

@Value(staticConstructor = "of")
public class UnidadDeTiempo {
    UnidadDeTiempoEnum unidadDeTiempoEnum;

    public UnidadDeTiempo(UnidadDeTiempoEnum unidadDeTiempoEnum) {
        Precondition.checkNotNull(unidadDeTiempoEnum);
        this.unidadDeTiempoEnum = unidadDeTiempoEnum;
    }
}
