package com.example.trainingfullstack.actividad2.domain;

import com.example.trainingfullstack.common.Precondition;
import lombok.Value;

@Value(staticConstructor = "from")
public class HoraDelDia {
    Hour hour;
    Minute minute;
    Second second;

    public HoraDelDia(Hour hour, Minute minute, Second second) {
        Precondition.checkNotNull(hour);
        Precondition.checkNotNull(minute);
        Precondition.checkNotNull(second);
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
}
