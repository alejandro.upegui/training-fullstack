package com.example.trainingfullstack.actividad2.domain;

import com.example.trainingfullstack.common.Precondition;
import lombok.Value;

@Value(staticConstructor = "of")
public class Hour {
    Integer value;

    private Hour(Integer value){
        Precondition.checkNotNull(value);
        Precondition.checkArgument(value >= 0 && value <= 23);
        this.value = value;
    }
}
