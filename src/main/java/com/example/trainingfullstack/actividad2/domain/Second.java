package com.example.trainingfullstack.actividad2.domain;

import com.example.trainingfullstack.common.Precondition;
import lombok.Value;

@Value(staticConstructor = "of")
public class Second {
    Integer value;

    public Second(Integer value) {
        Precondition.checkNotNull(value);
        Precondition.checkArgument(value >= 0 && value <= 59);
        this.value = value;
    }
}
