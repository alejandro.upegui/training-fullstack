package com.example.trainingfullstack.actividad2.domain;


import java.time.LocalTime;

public enum UnidadDeTiempoEnum{
    Milisegundos,
    Segundos,
    Minutos,
    Horas;

        public static Long fromHour(UnidadDeTiempoEnum unit, LocalTime hour){
            switch (unit){
                case Horas:
                    long HoursFinal = (long) (hour.getHour()+ (hour.getMinute()/60)+(hour.getSecond()/3600)+((hour.getSecond()*1000)/3.6e+3));
                    return HoursFinal;
                case Minutos:
                    long MinutesFinal = hour.getMinute()+(hour.getHour()*60)+(hour.getSecond()/60)+((hour.getSecond()*1000)/60000);
                    return MinutesFinal;
                case Segundos:
                    long SegundosFinal = hour.getSecond()+((hour.getSecond()*1000)/1000)+(hour.getHour()*3600) + (hour.getMinute()/60);
                    return SegundosFinal;
                case Milisegundos:
                    long MiliSegundosFinal = (long) (hour.getHour()*3.6e+5)+(hour.getSecond()*1000)+(hour.getSecond()*1000) + (hour.getMinute()*60000);
                    return MiliSegundosFinal;
                default:
                    throw new UnsupportedOperationException();
            }
        }


}
