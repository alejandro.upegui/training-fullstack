package com.example.trainingfullstack;

import com.example.trainingfullstack.configuration.dominioDatabaseCredentials;
import com.example.trainingfullstack.user.domain.UserAuthRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;

@SpringBootApplication(exclude = {JacksonAutoConfiguration.class})
public class TrainingFullstackApplication {

	dominioDatabaseCredentials dominioDatabaseCredentials;


	public static void main(String[] args) {
		SpringApplication.run(TrainingFullstackApplication.class, args);
	}

}
