package com.example.trainingfullstack;

import com.example.trainingfullstack.user.domain.UserAuthRequest;
import com.example.trainingfullstack.user.domain.UserName;
import com.example.trainingfullstack.user.serialization.UserNameAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonExample {
    public static void main(String[] args) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(UserName.class, new UserNameAdapter())
                .create();
        UserName userName = UserName.of("test-username");
        String password = "password...";
        UserAuthRequest request = UserAuthRequest.from(
                userName,
                password
        );
        System.out.println(gson.toJson(request));
        System.out.println(gson.fromJson("{\"username\":\"test-username\",\"password\":\"password...\"}",UserAuthRequest.class));
    }
}
