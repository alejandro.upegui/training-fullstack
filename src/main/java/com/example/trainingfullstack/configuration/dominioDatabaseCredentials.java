package com.example.trainingfullstack.configuration;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "database")
@RequiredArgsConstructor
@Data
public class dominioDatabaseCredentials {
    private String host;
    private String port;
    private String username;
    private String password;
    private String database;

}
