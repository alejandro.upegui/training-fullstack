package com.example.trainingfullstack.configuration;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "dayhour")
@RequiredArgsConstructor
@Data
public class CredentialsUnitTime {
    private String unitTime;
}
