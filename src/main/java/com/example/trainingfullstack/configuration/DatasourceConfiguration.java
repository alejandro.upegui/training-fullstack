package com.example.trainingfullstack.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Configuration
public class DatasourceConfiguration {

    @Bean
    @Profile({"test"})
    public DataSource testDataSource(){
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:scripts/schema.sql")
                .addScript("classpath:scripts/data.sql")
                .build();
    }

    @Bean
    public DataSource hikariDataSource(dominioDatabaseCredentials credentials){
        HikariConfig config = new HikariConfig();
        String JdbcUrl = String.format("jdbc:postgresql://%s:%s/%s",credentials.getHost(),credentials.getPort(),credentials.getDatabase());
        config.setJdbcUrl(JdbcUrl);
        config.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        config.setUsername(credentials.getUsername());
        config.setPassword(credentials.getPassword());
        return new HikariDataSource(config);
    }
}
