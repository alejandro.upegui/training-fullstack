package com.example.trainingfullstack.configuration;

import com.example.trainingfullstack.user.domain.UserName;
import com.example.trainingfullstack.user.serialization.UserNameAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GsonConfiguration {
    @Bean
    public Gson gson(){
        return new GsonBuilder()
                .registerTypeAdapter(UserName.class, new UserNameAdapter())
                .create();
    }
}
